import * as uuid from 'uuid';
import CartParser from './CartParser';
import path from 'path'

jest.mock('uuid');

const uuidSpy = jest.spyOn(uuid, 'v4');
const consoleSpy = jest.spyOn(console, "error");

let parser, parse, validate, parseLine, calcTotal, readFile;

beforeEach(() => {
    parser = new CartParser();
    parse = parser.parse.bind(parser)
    readFile = parser.readFile.bind(readFile)
    validate = parser.validate.bind(parser)
    calcTotal = parser.calcTotal.bind(parser)
    parseLine = parser.parseLine.bind(parser)
});

afterEach(() => {
    jest.clearAllMocks();
});

describe('CartParser - unit tests', () => {
    describe('calcTotal', () => {
        it('should return total price of all product in cart', () => {
            const cartItems = [
                {
                    "id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
                    "name": "Mollis consequat",
                    "price": 9,
                    "quantity": 2
                },
                {
                    "id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
                    "name": "Tvoluptatem",
                    "price": 10.32,
                    "quantity": 1
                },
                {
                    "id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
                    "name": "Scelerisque lacinia",
                    "price": 18.9,
                    "quantity": 1
                },
                {
                    "id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
                    "name": "Consectetur adipiscing",
                    "price": 28.72,
                    "quantity": 10
                },
                {
                    "id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
                    "name": "Condimentum aliquet",
                    "price": 13.9,
                    "quantity": 1
                }
            ]

            expect(calcTotal(cartItems)).toBeCloseTo(348.32, 2)
        });
    })

    describe('parseLine', () => {
        it('should return JSON object, which represents a cart item', () => {
            const csvLine = 'Mollis consequat,9.00,2'
            uuid.v4.mockImplementation(() => 'testid');

            expect(parseLine(csvLine)).toEqual({name: 'Mollis consequat', price: 9, quantity: 2, id: 'testid'});

            expect(uuidSpy).toHaveBeenCalledTimes(1);
        });
    })

    describe('validate', () => {
        it('should return an array of validation errors when header is different from the schema', () => {
            const contents = 'Prduct name,Price,Quantity\n' +
                'Mollis consequat,9.00,2\n' +
                'Tvoluptatem,10.32,1\n' +
                'Scelerisque lacinia,18.90,1\n' +
                'Consectetur adipiscing,28.72,10\n' +
                'Condimentum aliquet,13.90,1'
            expect(validate(contents)).toEqual([{
                type: 'header',
                row: 0,
                column: 0,
                message: 'Expected header to be named "Product name" but received Prduct name.'
            }]);
        });
    })

    describe('validate', () => {
        it('should return an array of validation errors when amnt of columns is different from the schema', () => {
            const contents = 'Product name,Price,Quantity\n' +
                'Mollis consequat,2\n'
            expect(validate(contents)).toEqual([{
                type: 'row',
                row: 1,
                column: -1,
                message: 'Expected row to have 3 cells but received 2.'
            }])
        });
    })

    describe('validate', () => {
        it('should return an array of validation errors when price is negative', () => {
            const contents = 'Product name,Price,Quantity\n' +
                'Mollis consequat,9.00,2\n' +
                'Tvoluptatem,10.32,1\n' +
                'Scelerisque lacinia,-18.90,1\n' +
                'Consectetur adipiscing,28.72,10\n' +
                'Condimentum aliquet,13.90,1'
            expect(validate(contents)).toEqual([{
                type: 'cell',
                row: 3,
                column: 1,
                message: 'Expected cell to be a positive number but received "-18.90".'
            }])
        });
    })

    describe('parse', () => {
        it('should throw Error with message Validation failed! if validationErrors.length > 0 and message in console', () => {

            parser.validate = jest.fn(() => [{
                type: 'cell',
                row: 3,
                column: 1,
                message: 'Expected cell to be a positive number but received "-18.90".'
            }]);

            try {
                parse(path.resolve('./samples/cart.csv'))
            } catch (error) {
                expect(error).toBeInstanceOf(Error);
                expect(error).toHaveProperty('message', 'Validation failed!');
            }
            expect(consoleSpy).toHaveBeenCalledTimes(1);
        });
    })

    describe('parse', () => {
        it('should return an object', () => {
            parser.readFile = jest.fn(() => 'Product name,Price,Quantity\n' +
                'Mollis consequat,9.00,2\n');

            expect(parse()).toEqual({
                items: [
                    { name: 'Mollis consequat', price: 9, quantity: 2, id: undefined }
                ],
                total: 18
            })
        });
    })
});

describe('CartParser - integration test', () => {
    // Add your integration test here.
});